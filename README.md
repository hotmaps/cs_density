# cs_density




## Repository structure
```
data                    -- containts the dataset in Geotiff format
readme.md               -- Readme file 
datapackage.json        -- Includes the meta information of the dataset for processing and data integration

```


## Documentation

This dataset contains the density of charging station per hectare over Europe. The charging sations locations were collected from OpenChargeMap and Opwndata.swiss.



## Limitations of data

The data containes the publicly accessible charge infrastructures declared on the websites before july, 11th 2022. Private charge point or public stations added after this date are not included. Only the position of the charging station is taken into account, the other caracteristics are note provided.

### References 
- EU28: [Open Charge map](https://openchargemap.org/site/develop/api#/)
- Switzwerland: [Opendata.swiss](https://opendata.swiss/fr/dataset/ladestationen-fuer-elektroautos)

Accesed on july, 11th 2022.


## How to cite
Jeannin Noémie, OPENGIS4ET Project WP3


## Authors
Jeannin Noémie <sup>*</sup>

<sup>*</sup> [EPFL, PV-Lab](https://www.epfl.ch/labs/pvlab/), Laboratory of photovoltaics and thin-films electronics, Rue de la Maladière 71b, CH-2002 Neuchâtel 2


## License
Copyright © 2023: Noémie Jeannin
 
Creative Commons Attribution 4.0 International License
This work is licensed under a Creative Commons CC BY 4.0 International License.

SPDX-License-Identifier: CC-BY-4.0

License-Text: https://spdx.org/licenses/CC-BY-4.0.html

## Acknowledgment
We would like to convey our deepest appreciation to the OPENGIS4ET Project, which provided the funding to carry out the present investigation.



